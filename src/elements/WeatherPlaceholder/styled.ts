import styled from "styled-components/native";
import WeatherPlaceholderIcon from "../../svgComponents/WeatherPlaceholderIcon";

const StyledWeatherPlaceholderIcon = styled(WeatherPlaceholderIcon)``;

export { StyledWeatherPlaceholderIcon };
