import styled from "styled-components/native";
import { DESIGN_VARS } from "../../constants";

const StyledH2 = styled.Text`
  color: ${DESIGN_VARS.color.darkBlue};
  /* font-family: UbuntuBold; */
  font-size: ${DESIGN_VARS.fontSize.h2};
  line-height: ${DESIGN_VARS.lineHeight.h2};
`;

export { StyledH2 };
