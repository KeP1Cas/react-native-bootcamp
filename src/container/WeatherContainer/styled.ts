import styled from "styled-components/native";
import { DESIGN_VARS } from "../../constants";

import ChevronLeft from "../../svgComponents/ChevronLeft";
import ChevronRight from "../../svgComponents/ChevronRight";

const WeatherContainerContainer = styled.View`
  background-color: ${DESIGN_VARS.color.white};
  /* box-shadow: 0px 4px 4px rgba(4, 5, 73, 0.25),
    14px 14px 20px rgba(5, 6, 114, 0.2); */
  border-radius: 8px;
  min-height: 524px;
  margin-top: 50px;
`;

const WeatherContainerContent = styled.View`
  padding: 32px 24px 63px;
  min-width: 300px;
`;

const ControlsContainer = styled.View`
  display: flex;
  justify-content: space-between;
  align-items: center;
`;

const WeatherPlaceholderContainer = styled.View`
  padding: 25px 0px 24px;
  margin: 0 auto;
`;

const CardTitleContainer = styled.View`
  margin: 0 auto 32px;
  text-align: center;
`;

const CardPromptContainer = styled.View`
  width: 100%;
  display: flex;
  justify-content: center;
  /* white-space: pre; */
`;

const WeatherCardsContainer = styled.View`
  margin-top: 54px;
  margin: 0 auto;
  display: flex;
  position: relative;
  max-width: 260px;
  /* overflow-x: auto; */
`;

const StyledChevronLeft = styled(ChevronLeft)`
  position: absolute;
  top: 50%;
  /* transform: translate(-100%, -50%); */
  left: -10px;
`;

const StyledChevronRight = styled(ChevronRight)`
  position: absolute;
  top: 50%;
  /* transform: translate(100%, -50%); */
  right: -10px;
`;

export {
  WeatherContainerContainer,
  WeatherContainerContent,
  ControlsContainer,
  WeatherPlaceholderContainer,
  CardTitleContainer,
  CardPromptContainer,
  WeatherCardsContainer,
  StyledChevronRight,
  StyledChevronLeft,
};
