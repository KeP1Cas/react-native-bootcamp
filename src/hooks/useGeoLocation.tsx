import React, { useEffect, useState } from "react";

type PropsGeo = {
  loaded: boolean;
  coordinates?: any;
  error?: any;
};

const useGeoLocation = () => {
  const [location, setLocation] = useState<PropsGeo>({
    loaded: false,
    coordinates: { lat: null, lon: null },
  });

  const onSuccess = (location: any) => {
    setLocation({
      loaded: true,
      coordinates: {
        lat: location.coords.latitude,
        lon: location.coords.longitude,
      },
    });
  };

  const onError = (error: { code: number; message: string }) => {
    setLocation({
      loaded: true,
      error,
    });
  };

  useEffect(() => {
    if (!("geolocation" in navigator)) {
      onError({
        code: 0,
        message: "Geolocation not supported",
      });
    }

    navigator.geolocation.getCurrentPosition(onSuccess, onError);
  }, []);

  return location;
};

export default useGeoLocation;
