import React from "react";
import Svg, { Path } from "react-native-svg";
const Chevron = () => {
  return (
    <Svg width="16" height="16" viewBox="0 0 16 16" fill="none">
      <Path
        fill-rule="evenodd"
        clip-rule="evenodd"
        d="M13.2929 4.29297L14.7071 5.70718L8.00001 12.4143L1.29291 5.70718L2.70712 4.29297L8.00001 9.58586L13.2929 4.29297Z"
        fill="#8083A4"
      />
    </Svg>
  );
};

export default Chevron;
