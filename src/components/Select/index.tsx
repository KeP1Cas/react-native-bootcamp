import React, { useState, useRef } from "react";
import {
  Pressable,
  ScrollView,
  ScrollViewComponent,
  Text,
  TouchableHighlight,
  TouchableWithoutFeedback,
  View,
  ViewComponent,
  TouchableOpacity,
} from "react-native";

import {
  StyledSelect,
  SelectContainer,
  // StyledChevron,
  SelectDropdown,
  SelectOption,
  SelectDropdownContainer,
} from "./styled";

export type SelectValue = {
  value: string | number;
  title: string | number;
};

type Props = {
  options: SelectValue[];
  placeholder: string;
  onChange: (value: any) => void;
  value: string;
};

const Select: React.FC<Props> = ({ options, placeholder, onChange, value }) => {
  const [isActive, setIsActive] = useState(false);

  const selectContainerRef = useRef({} as ViewComponent);

  /**
   * Дело в том, что дропдаун уходит в display: none из-за onBlur, раньше, чем обработается клик по SelectOption,
   * поэтому необходима небольшая задержка, перед тем, как дропдаун исчезнет. Проблема заключается в упрощенной реализации скрытия дропдауна,
   * будь у меня больше времени - я бы реализовал скрытие дропдауна через прослушку кликов вне элемента через document.addEventListener("click" ...)
   * Я попробовал, но столкнулся с некоторыми сложностями в работе моего решения "на скорую руку" через вышеупомянутый способ
   */
  const onBlur = () => {
    setTimeout(() => {
      setIsActive(false);
    }, 200);
  };

  const onClickOption = (value: any) => {
    onChange(value);
  };

  return (
    <TouchableHighlight
      onPress={() => setIsActive((prev) => !prev)}
      onBlur={onBlur}
    >
      <SelectContainer ref={selectContainerRef}>
        <StyledSelect placeholder={placeholder} value={`${value}`} />
        <SelectDropdownContainer isActive={isActive}>
          <SelectDropdown>
            <ScrollView horizontal={false}>
              {options.map(({ title, value: optionValue }, idx) => (
                <TouchableHighlight onPress={() => onClickOption(optionValue)}>
                  <SelectOption isActive={optionValue === value} key={idx}>
                    {title && <Text>{title}</Text>}
                  </SelectOption>
                </TouchableHighlight>
              ))}
            </ScrollView>
          </SelectDropdown>
        </SelectDropdownContainer>
        {/* <StyledChevron isActive={isActive} /> */}
      </SelectContainer>
    </TouchableHighlight>
  );
};

export default Select;
