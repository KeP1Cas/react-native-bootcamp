import React, { useState, useEffect } from "react";

import { OfflineMode, OfflineModeIcon } from "./styled";

const OnlineMode = () => {
  const [onlineMode, setOnlineMode] = useState(true);

  function handleNetworkChange(): void {
    if (navigator.onLine) {
      setOnlineMode;
    } else {
      setOnlineMode(false);
    }
  }

  useEffect(() => {
    handleNetworkChange();
    window.addEventListener("online", handleNetworkChange);
    window.addEventListener("offline", handleNetworkChange);
  }, []);

  return (
    <OfflineMode>
      {!onlineMode && (
        <OfflineModeIcon source={{ uri: "../../assets/icons/no-signal.png" }} />
      )}
    </OfflineMode>
  );
};

export default OnlineMode;
