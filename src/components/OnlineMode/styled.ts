import styled from "styled-components/native";

const OfflineMode = styled.View`
  position: fixed;
  right: 20px;
  top: 20px;
  z-index: 3;
`;

const OfflineModeIcon = styled.Image`
  width: 30px;
  height: 30px;
`;

export { OfflineMode, OfflineModeIcon };
