import React, { useState, useRef } from "react";
import { TouchableHighlight } from "react-native";

import {
  StyledDatepicker,
  DatepickerContainer,
  StyledCalendarIcon,
} from "./styled";

type Props = {
  onChange: (date: string) => void;
  value: string;
};

const Datepicker: React.FC<Props> = ({ onChange, value }) => {
  const [hasFocus, setHasFocus] = useState(false);

  const textInputRef = useRef({} as HTMLInputElement);

  const onClickCalendarIcon = () => {
    if (textInputRef.current) {
      textInputRef.current.focus();
      setHasFocus(true);
    }
  };

  const onFocus = () => {
    setHasFocus(true);
  };

  const onBlur = () => {
    setHasFocus(false);
  };

  const onChangeInner = (e: React.ChangeEvent<HTMLInputElement>) => {
    onChange(e.target.value);
  };

  const getTextInputDateFormat = (dateString?: string) => {
    if (!dateString) {
      return;
    }

    const [year, month, day] = dateString.split("-");

    return `${month} / ${day} / ${year}`;
  };

  const valueInner = hasFocus ? value : getTextInputDateFormat(value);

  const today = new Date().toISOString().split("T")[0];
  const pastWeekDate = new Date(new Date().getTime() - 4 * 24 * 60 * 60 * 1000)
    .toISOString()
    .split("T")[0];

  return (
    <DatepickerContainer>
      <TouchableHighlight onFocus={onFocus} onBlur={onBlur}>
        <StyledDatepicker
          ref={textInputRef}
          onChange={onChangeInner}
          placeholder="Select Date"
          value={valueInner as any}
          type={hasFocus ? "date" : "text"}
          min={pastWeekDate}
          max={today}
        />
        {/* <TouchableHighlight onPress={onClickCalendarIcon}>
          <StyledCalendarIcon hasFocus={hasFocus} />
        </TouchableHighlight> */}
      </TouchableHighlight>
    </DatepickerContainer>
  );
};

export default Datepicker;
