import styled from "styled-components/native";

const StyledAppBackground = styled.View`
  width: 100%;
  height: 100%;
`;

export { StyledAppBackground };
