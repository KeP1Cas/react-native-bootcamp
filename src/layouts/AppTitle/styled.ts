import styled from "styled-components/native";
import { DESIGN_VARS } from "../../constants";

const AppTitleContainer = styled.View`
  display: flex;
  justify-content: center;
  align-items: center;
  font-size: 54px;

  /* font-family: UbuntuBold; */
`;

const AppPartTitleWeather = styled.Text`
  /* grid-area: weathertitle; */
  font-size: 40px;
  color: ${DESIGN_VARS.color.white};
  position: relative;
  left: -40px;
`;

const AppPartTitleForecast = styled.Text`
  /* grid-area: forecasttitle; */
  position: relative;
  left: 46px;
  font-size: 40px;
  color: ${DESIGN_VARS.color.white};
`;

// const AppPartTitleEmpty = styled.View`
//   grid-area: epmty;
// `;

export {
  AppPartTitleForecast,
  AppPartTitleWeather,
  AppTitleContainer,
  // AppPartTitleEmpty,
};
