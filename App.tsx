import React from "react";
import AppBackground from "./src/layouts/AppBackground";
import {
  AppTitleMainContainer,
  WeatherContainersContainer,
  ILoveMercuryContainer,
  BodyView,
} from "./styled";
import AppTitle from "./src/layouts/AppTitle";
import WeatherContainer from "./src/container/WeatherContainer";
import Meta from "./src/elements/Meta";
import OnlineMode from "./src/components/OnlineMode";
import { ScrollView } from "react-native";

const App = () => {
  return (
    <BodyView>
      <ScrollView horizontal={false}>
        <AppBackground>
          <AppTitleMainContainer>
            <AppTitle />
            {/* <OnlineMode /> */}
          </AppTitleMainContainer>
          <WeatherContainersContainer>
            <WeatherContainer isSevenDaysCard />
            <WeatherContainer />
          </WeatherContainersContainer>
          <ILoveMercuryContainer>
            <Meta uppercase>C ЛЮБОВЬЮ ОТ MERCURY DEVELOPMENT</Meta>
          </ILoveMercuryContainer>
        </AppBackground>
      </ScrollView>
    </BodyView>
  );
};

export default App;
