import styled from "styled-components/native";
import { DESIGN_VARS } from "./src/constants";

const AppTitleMainContainer = styled.View`
  width: 100%;
  display: flex;
  justify-content: center;
  padding-top: 30px;
  margin-top: 40px;
`;

const BodyView = styled.View`
  background-color: #373af5;
  width: 100%;
  padding: 0;
  margin: 0;
`;

const WeatherContainersContainer = styled.View`
  width: 100%;
  display: flex;
  justify-content: center;
`;

const ILoveMercuryContainer = styled.View`
  flex: 0 0 auto;
  margin: 0 auto;
  text-align: center;
  margin: 100px 0 16px;
  font-size: 20px;
`;

export {
  AppTitleMainContainer,
  WeatherContainersContainer,
  ILoveMercuryContainer,
  BodyView,
};
